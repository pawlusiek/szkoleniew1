package com.developers.pawlusiek.aplikacja01;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class WidokiActivity extends ActionBarActivity {

    // dla loggera
    private static final String TAG = WidokiActivity.class.getName();
    private static final String LISTA_STRINGOW = "listaStringow";

    // widoki jest dobrze trzymac jako pola prywatne
    private EditText mEditText;
    private TextView mTextView;
    private Button mButton;
    private LinearLayout mLinear;
    private ArrayList<String> mZapisanyStan;

    @Override
    // ustawiamy jaki wiok ma być wywołany dla aktywnosci
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // odwolujemy sie do layoutów i konkretnego layoutu, którego miejsce w pamieci okresla R


        // zamiast ladowac kontext z xml'a
        setContentView(R.layout.activity_widoki);


        // uzyjemy kodu
        // setContentView(R.layout.activity_widoki_zkodu);      // --> zamiaste tego mozemy ładwać kontekst w funkcji widokiZkodu
        // mTextView = (TextView) findViewByIHd(R.id.p_label);

        // widokiZkodu();


        // to samo za pomoca findView ponizej
        mEditText = findView(R.id.p_pole_tekstowe);
        mButton = findView(R.id.p_przycisk_gotowe);
        mTextView = findView(R.id.p_label);
        mLinear = findView(R.id.p_linear_layout);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);
                // nie klepac dalej kodu ! -> to tylko sobie woła cos, co zdefinowane jest gdzies indziej
            }
        });


    }

    // przydatna funkcyjka zeby pominac rzutowniaa
    private <T extends View> T findView(int identyfikator) {
        return (T) findViewById(identyfikator);
    }

    public void klikPrzycisk(View view) {
        LinearLayout.LayoutParams mControlsParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        String text = mEditText.getText().toString();
        Log.d(TAG, text);
        // wyswietlanie komunikatu
        if (text == null || text.isEmpty()) {
            Toast.makeText(this, text != null ? R.string.s_toast_pusty : R.string.s_toast_krotki, Toast.LENGTH_LONG).show();
            Log.d(TAG, text == null ? getResources().getString(R.string.s_toast_pusty) : getResources().getString(R.string.s_toast_krotki));
            return;
        }
        mEditText.setText("");
        // mTextView.setText(text);

        TextView mNewTextView = new TextView(this);
        mNewTextView.setLayoutParams(mControlsParams);
        mNewTextView.setText(text);
        mLinear.addView(mNewTextView);

    }

    public void widokiZkodu() {
        /* podajemy kontekst
        aktywnosc -> wybieramy this bo jestesmy w kodzie aplikacji
        serwis
        aplikacja
        */
        // Tworzenie layoutu
        //mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        // tworznie konfiguracji layoutu
        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        // ustawanie Layoutu
        mLinear.setLayoutParams(mParams);

        // -----------------------------------------------------------------------------------------------------------
        // ustawnia
        LinearLayout.LayoutParams mControlsParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // pole tekstowe
        mEditText = new EditText(this);
        mEditText.setLayoutParams(mControlsParams);
        mEditText.setSingleLine();
        mEditText.setHint(R.string.s_pole_tekstowe);

        // text View
        mTextView = new TextView(this);
        mTextView.setLayoutParams(mControlsParams);


        // button
        mButton = new Button(this);
        LinearLayout.LayoutParams mControlsParamsButton = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mButton.setLayoutParams(mControlsParamsButton);
        mButton.setText(R.string.s_przycisk_gotowe);

        //
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikPrzycisk(v);
                // nie klepac dalej kodu ! -> to tylko sobie woła cos, co zdefinowane jest gdzies indziej
            }
        });

        // dodawanie komponentow na strone

        mLinear.addView(mEditText);
        mLinear.addView(mTextView);
        mLinear.addView(mButton);

        // odwolujemy sie do kontener -> czyli glownego kontenera z XMLa, ale przed wywolaniem tego musimy ustawic setContentView na jakis XML
        //((RelativeLayout) findViewById(R.id.kontener)).addView(mLinear);

        // jezeli chcemy pominac plik XML mozemy uzyc samego
        setContentView(mLinear);
    }

    public void wyczysc() {

        List<View> mKolekcjaDoUsuniecia = new ArrayList<View>();
        for (int i = 0; i < mLinear.getChildCount(); i++) {
            View mChild = mLinear.getChildAt(i);
            mKolekcjaDoUsuniecia.add(mChild);
        }

        for (View mChild : mKolekcjaDoUsuniecia) {
            if (mChild.getClass().equals(TextView.class)) {
                if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button))
                    mLinear.removeView(mChild);
            }
        }
        // nie dziala bo ilosc Childów zmienia sie z kazdym wywolaniem
//
//        for (int i =0; i < mLinear.getChildCount(); i++){
//            View mChild = mLinear.getChildAt(i);
//            if (mChild.getClass().equals(TextView.class))
//                mLinear.removeView(mChild);
//            TextView mHelper = (TextView) mChild;
//            Log.d(TAG, "usunieto: " + mHelper.getText());
//        }
    }

    public void wyczyscOstatni() {
//    int[] idki= mLinear.getDrawableState();
//        //mLinear.removeView();
//        for (int i : idki){
//            Log.d(TAG, "p = " + i);
//        }
        View mChild = mLinear.getChildAt(mLinear.getChildCount() - 1);
        if (mChild.getClass().equals(TextView.class)) {
            if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button))
                mLinear.removeView(mChild);
        }
    }

    // to jest menu na gorze
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }

    // obsluga tego menu na gorze
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.p_usun_wszysko) {
            wyczysc();
        }

        if (id == R.id.p_usun_ostatni) {
            wyczyscOstatni();
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // zapisywanie stanu w kolekcji

        if (mZapisanyStan == null)
            mZapisanyStan = new ArrayList<String>();

        List<View> mWidokiZapisane = new ArrayList<View>();

        for (int i = 0; i < mLinear.getChildCount(); i++) {
            View mChild = mLinear.getChildAt(i);
            mWidokiZapisane.add(mChild);
        }

        for (View mChild : mWidokiZapisane) {
            if (mChild.getClass().equals(TextView.class)) {
                if (mChild instanceof TextView && !(mChild instanceof EditText) && !(mChild instanceof Button))
                    mZapisanyStan.add(((TextView) mChild).getText().toString());
            }
        }

            savedInstanceState.putStringArrayList(LISTA_STRINGOW,mZapisanyStan);


        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // zapisywanie stanu w kolekcji

        for(String s : savedInstanceState.getStringArrayList(LISTA_STRINGOW)){
            TextView mChild = new TextView(this);
            mChild.setText(s);
            mLinear.addView(mChild);
        }
        mZapisanyStan = null;

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }



}
